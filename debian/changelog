libmediaart (1.9.6-2+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Wed, 26 Feb 2025 14:34:28 +0000

libmediaart (1.9.6-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Add debian/upstream/metadata

  [ Jeremy Bícha ]
  * Add ${gir:Depends} & ${gir:Provides} to -dev package
  * Enable all hardening flags
  * debian/libmediaart-2.0-0.symbols: Set Build-Depends-Package
  * Opt into dpkg build API v1
  * Opt into Salsa CI
  * Bump Standards Version to 4.7.0

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 16 Jan 2025 13:28:28 -0500

libmediaart (1.9.6-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Tue, 28 Mar 2023 12:05:30 +0000

libmediaart (1.9.6-1) unstable; urgency=medium

  * Team upload
  * New upstream release

 -- Nathan Pratta Teodosio <nathan.teodosio@canonical.com>  Thu, 02 Jun 2022 09:45:17 -0300

libmediaart (1.9.5-2) unstable; urgency=medium

  * debian/rules: fix build option typo (Closes: #998521)

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 28 Nov 2021 14:33:43 -0500

libmediaart (1.9.5-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Build with meson
  * debian/libmediaart-2.0-dev.install: Install vala .deps file
  * Bump debhelper-compat to 13
  * Build-Depend on dh-sequence-gir

 -- Jeremy Bicha <jbicha@debian.org>  Mon, 23 Aug 2021 19:45:37 -0400

libmediaart (1.9.4-3+apertis1) apertis; urgency=medium

  * Refresh the automatically detected licensing information

 -- Walter Lozano <walter.lozano@collabora.com>  Thu, 19 Aug 2021 06:13:50 -0300

libmediaart (1.9.4-3apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 08:55:32 +0000

libmediaart (1.9.4-3) unstable; urgency=medium

  * Team upload

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field

  [ Simon McVittie ]
  * Change Maintainer to GNOME team at maintainer's request
  * Preferentially build-depend on libgdk-pixbuf-2.0-dev.
    We don't need the deprecated Xlib integration that is also pulled in
    by the older libgdk-pixbuf2.0-dev package (see #974870).
    (Closes: #976032)
  * d/control: Trim trailing whitespace
  * Set debhelper-compat version in Build-Depends
  * Remove Section on libmediaart-2.0-0 that duplicates source
  * d/upstream/metadata: Add
  * d/tests: Add a superficial autopkgtest

 -- Simon McVittie <smcv@debian.org>  Sat, 28 Nov 2020 16:45:05 +0000

libmediaart (1.9.4-2co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to target

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sat, 20 Feb 2021 01:15:10 +0000

libmediaart (1.9.4-2) unstable; urgency=medium

  * Update Vcs-* to point to salsa.debian.org (gitlab)
  * Bump debhelper compat level to 11
  * Switch to dh_missing and abort on uninstalled files

 -- Michael Biebl <biebl@debian.org>  Thu, 31 May 2018 02:04:31 +0200

libmediaart (1.9.4-1) unstable; urgency=medium

  * New upstream version 1.9.4
  * Drop debian/patches/0001-extract-Add-missing-config.h-includes.patch,
    merged upstream

 -- Michael Biebl <biebl@debian.org>  Wed, 23 Aug 2017 15:08:16 +0200

libmediaart (1.9.3-1) unstable; urgency=medium

  * New upstream version 1.9.3
  * Drop debian/patches/0001-build-hide-private-symbols.patch, merged upstream
  * Add missing config.h includes.
    This was making the functions defined in extractgeneric.h not really
    exported.
  * Switch debhelper compat level to 10 for automatic dh-autoreconf
  * Drop obsolete --disable-nemo configure switch
  * Run the upstream test suite during the build and not via autopkgtest
  * Bump Standards-Version to 4.0.1

 -- Michael Biebl <biebl@debian.org>  Thu, 10 Aug 2017 21:47:02 +0200

libmediaart (1.9.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Michael Biebl <biebl@debian.org>  Sat, 23 May 2015 17:13:17 +0200

libmediaart (1.9.0-1) experimental; urgency=medium

  * New upstream release.
  * Rename binary packages for the API version bump from 1.0 to 2.0.
  * Add patch which hides the private symbols from the internal API.
  * Force a rebuild of marshal.[ch].
  * Start shipping a symbols file which replaces the shlibs file.
  * Update Vcs-Browser to use cgit and https and use the canonical Vcs-* URLs.

 -- Michael Biebl <biebl@debian.org>  Fri, 01 May 2015 09:25:31 +0200

libmediaart (0.7.0-2) unstable; urgency=medium

  [ Tim Lunn ]
  * debian/rules: add --with gir to ensure dh_girepository is run
  * debian/tests: run unit-tests as an autopkgtest (Closes: #765008)

  [ Michael Biebl ]
  * Install typelib files into multiarch paths.
  * Mark gir and dev package as Multi-Arch: same.
  * Bump Standards-Version to 3.9.6. No further changes.

 -- Michael Biebl <biebl@debian.org>  Tue, 14 Oct 2014 01:10:16 +0200

libmediaart (0.7.0-1) unstable; urgency=medium

  * New upstream release.
  * Add a shlibs file for libmediaart-1.0-0 which generates a tight
    dependency. API and ABI are not stable yet.
  * Ship vala bindings.

 -- Michael Biebl <biebl@debian.org>  Tue, 23 Sep 2014 22:19:25 +0200

libmediaart (0.4.0-2) unstable; urgency=medium

  [ Andreas Barth ]
  * Use dh-autoreconf to fix FTBFS on ppc64el. (Closes: #757211)

 -- Michael Biebl <biebl@debian.org>  Tue, 09 Sep 2014 12:54:11 +0200

libmediaart (0.4.0-1) unstable; urgency=medium

  * New upstream release.

 -- Michael Biebl <biebl@debian.org>  Tue, 01 Apr 2014 20:28:48 +0200

libmediaart (0.3.0-1) unstable; urgency=medium

  * New upstream release.
  * Upload to unstable.

 -- Michael Biebl <biebl@debian.org>  Sat, 08 Mar 2014 15:31:46 +0100

libmediaart (0.2.0-1) experimental; urgency=medium

  * Initial release.

 -- Michael Biebl <biebl@debian.org>  Fri, 21 Feb 2014 01:34:32 +0100
